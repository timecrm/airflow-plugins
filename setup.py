from setuptools import setup, find_packages

setup(
    name="mapp-airflow-extensions",
    descriptions="Airflow extensions used my MateusApp Data Team",
    version='0.2.14c',
    author='Caio Belfort',
    author_email='caiobelfort90@gmail.com',
    packages=find_packages(exclude=['tests']),
    license='GPL'
)
