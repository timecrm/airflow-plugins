import datetime
import json
from tempfile import NamedTemporaryFile
from unittest import mock

import pandas
import pytest
import time

from mapp.operators.dv_operators import (HubFormatterOperator,
                                         _get_key_values_as_lists,
                                         _convert_arr_to_str_arr,
                                         SatelliteFormatterOperator,
                                         LinkFormatterOperator,
                                         DatavaultForeignKeyCheckOperator,
                                         generate_hash_from_str_list,
                                         generate_hash_from_keys)


class TestFNGenerateHashFromStrList(object):

    def test_return_correct(self):
        str_list1 = ['1738', 'Pedro']
        str_list2 = ['Pedro', '1738']
        expected = '649a547f5fcd8e6f2347394da89d5693'
        assert generate_hash_from_str_list(str_list1) == expected
        assert generate_hash_from_str_list(str_list2) == expected


class TestFnGenerateHashFromKeys(object):

    def test_return_correct_hash_single_key(self):
        row = {'code': 1738,
               'name': 'Pedro'}

        expected_hash = 'ffc4c75032186d49511d352957d1ac4f'
        expected_intermediary_hash = ['84c6494d30851c63a55cdb8cb047fadd']
        expected_key_value = [[1738]]

        result_hash, result_intermediary_hashs, result_key_value = generate_hash_from_keys(row, ['code'])

        assert result_intermediary_hashs == expected_intermediary_hash
        assert result_hash == expected_hash, 'Hashs are not equal.'
        assert result_key_value == expected_key_value, 'Key Values are not equal'

    def test_return_correct_hash_multiple_keys(self):
        row = {'customer_code': 1738,
               'date': '1996-10-03',
               'promotion_code': 329}

        e_intermediary_hashs = ['84c6494d30851c63a55cdb8cb047fadd', '6faa8040da20ef399b63a72d0e4ab575']
        e_final_hash = '5b2c8d57381799a43a085567c11c4ea3'
        e_keys = [[1738], [329]]

        result_hash, result_intermediary_hashs, result_key_value = generate_hash_from_keys(row, ['customer_code',
                                                                                                 'promotion_code'])

        assert result_intermediary_hashs == e_intermediary_hashs
        assert result_hash == e_final_hash, 'Hashs are not equal.'
        assert result_key_value == e_keys, 'Key Values are not equal'

    def test_return_correct_single_composite_key(self):
        row = {
            'ticket_number': 10,
            'store_id': 18,
            'date': '2018-10-01',
        }

        result_hash, result_intermediary_hashs, result_key_value = generate_hash_from_keys(row, [[
            'date',
            'store_id',
            'ticket_number'
        ]])

        e_intermediary_hashs = ['b188083c8c8b7c4954198dff896899c0']
        e_final_hash = '5f1dee9a651aae896728648516d48ccd'
        e_keys = [['2018-10-01', 18, 10]]

        assert result_intermediary_hashs == e_intermediary_hashs
        assert result_hash == e_final_hash, 'Hashs are not equal.'
        assert result_key_value == e_keys, 'Key Values are not equal'

    def test_return_correct_complex_key(self):
        row = {
            'ticket_number': 10,
            'store_id': 18,
            'date': '2018-10-01',
            'customer_code': 1738,
            'promotion_code': 329
        }

        e_intermediary_hashs = ['b188083c8c8b7c4954198dff896899c0',
                                '84c6494d30851c63a55cdb8cb047fadd',
                                '6faa8040da20ef399b63a72d0e4ab575']

        e_final_hash = 'a823d50797bbcab1b4ef8eba56168d9b'
        e_keys = [['2018-10-01', 18, 10], [1738], [329]]

        result_hash, result_intermediary_hashs, result_key_value = generate_hash_from_keys(row, [
            ['date', 'store_id', 'ticket_number'],
            ['customer_code'],
            ['promotion_code']
        ])

        assert result_intermediary_hashs == e_intermediary_hashs
        assert result_hash == e_final_hash, 'Hashs are not equal.'
        assert result_key_value == e_keys, 'Key Values are not equal'

    def test_return_equal_distinct_order(self):
        row = {
            'ticket_number': 10,
            'store_id': 18,
            'date': '2018-10-01',
            'customer_code': 1738,
            'promotion_code': 329
        }

        r1 = generate_hash_from_keys(row, [
            ['date', 'store_id', 'ticket_number'],
            ['customer_code'],
            ['promotion_code']
        ])

        r2 = generate_hash_from_keys(row, [
            ['customer_code'],
            ['promotion_code'],
            ['store_id', 'ticket_number', 'date']
        ])

        assert r1[0] == r2[0]


class TestFunctionKeyValueAsLists(object):

    def test_return_list_of_lists(self):
        """Tests if the function returns a list of lists"""

        row = {"bk1": 'val', "bk2": "val", "bk3": "val", "bk4": "val", "bk5": "val"}

        bk = [["bk1"], "bk2", ["bk3", "bk4"], "bk5"]

        formatted_bks = _get_key_values_as_lists(row, bk)

        # Tests if returned result is a list
        assert isinstance(formatted_bks, list)

        # Testes if each object in list is also a list
        assert all(isinstance(x, list) for x in formatted_bks)

    def test_correct_result(self):
        row = {"bk1": 2, "bk2": 3, "bk3": "val", "bk4": "val", "bk5": "val"}

        bk = [["bk1"], "bk2", ["bk3", "bk4"], "bk5"]

        formatted_bks = _get_key_values_as_lists(row, bk)

        expect = [[2], [3], ["val", "val"], ["val"]]

        assert formatted_bks == expect


class TestFunctionConvertArrToStrArr(object):
    def test_correct_conversion(self):
        input = [[2], [3], ["val", "val"], ["val"]]

        expected_output = [['2'], ['3'], ['val', 'val'], ['val']]

        assert expected_output == _convert_arr_to_str_arr(input)


class TestHubFormatterOperator(object):
    rows = [
        {'x': 3, 'y': 'val', 'z': 54},
        {'x': 1, 'y': 'lav', 'z': 108},
        {'x': 2, 'y': 'val', 'z': 33},
        {'x': 3, 'y': 'val', 'z': 54}  # Duplicate of first value
    ]

    schema = [
        {'name': 'hash', 'type': 'STRING'},
        {'name': 'load_date', 'type': 'TIMESTAMP'},
        {'name': 'record_source', 'type': 'STRING'},
        {'name': 'x', 'type': 'INTEGER'},
        {'name': 'y', 'type': 'STRING'},
        {'name': 'z', 'type': 'INTEGER'}
    ]

    input_bucket = 'gs://test'
    output_bucket = 'gs://test'

    input_object = 'input_file'
    output_object = 'output_file'

    business_keys = [['x', 'y', 'z']]
    load_date = '2018-10-01'
    record_source = 'test'

    @mock.patch('mapp.operators.dv_operators._get_data_from_gcs')
    @mock.patch('mapp.operators.dv_operators._send_data_to_gcs')
    def test_correct_result(self, mock_send_data_to_gcs, mock_get_data_from_gcs):
        tmp_file_download = NamedTemporaryFile(delete=False)

        for row in self.rows:
            s = json.dumps(row, sort_keys=True)
            s = s.encode('utf-8')
            tmp_file_download.write(s)
            tmp_file_download.write(b'\n')
        tmp_file_download.flush()

        mock_get_data_from_gcs.return_value = tmp_file_download.name

        ld = time.mktime(datetime.datetime.strptime(self.load_date, '%Y-%m-%d').timetuple())
        expected_values = [
            {'hash': '4381762de0a9a0eb3dd21ab1eac02f30', 'load_date': ld,
             'record_source': self.record_source, 'x': 3, 'y': 'val', 'z': 54},
            {'hash': '05dce42b0d7fe0f274448e02c1cbf673', 'load_date': ld,
             'record_source': self.record_source, 'x': 1, 'y': 'lav', 'z': 108},
            {'hash': '9d874d0b78dba5ae768fa71d27b17c59', 'load_date': ld,
             'record_source': self.record_source, 'x': 2, 'y': 'val', 'z': 33}
        ]

        def _assert_send_correct_file_to_gcs(gcp_conn_id, bucket, output, tmp_filename):
            assert bucket, self.output_bucket
            assert output, self.output_object

            with open(tmp_filename) as f:
                assert pandas.DataFrame(expected_values).to_json(orient='records', lines=True) == f.read()

        mock_send_data_to_gcs.side_effect = _assert_send_correct_file_to_gcs

        op = HubFormatterOperator(
            task_id='test-hub-formatter',
            input_bucket=self.input_bucket,
            output_bucket=self.input_bucket,
            input_object=self.input_object,
            output_object=self.output_object,
            business_keys=self.business_keys,
            load_date=self.load_date,
            record_source=self.record_source,
            schema=[field['name'] for field in self.schema]
        )

        op.execute(None)

        tmp_file_download.close()

    def test_throw_error_business_key_larger_than_one(self):
        with pytest.raises(RuntimeError, match="Hub business key list must be of size 1."):
            op = HubFormatterOperator(
                task_id='test-hub-formatter',
                input_bucket=self.input_bucket,
                output_bucket=self.input_bucket,
                input_object=self.input_object,
                output_object=self.output_object,
                business_keys=['x', 'y'],
                load_date=self.load_date,
                record_source=self.record_source,
                schema=[field['name'] for field in self.schema]
            )


class TestLinkFormatterOperator(object):
    rows = [
        {'x': 3, 'y': 'val', 'z': 54, 'm': 'test'},
        {'x': 1, 'y': 'lav', 'z': 108, 'm': 'test'},
        {'x': 2, 'y': 'val', 'z': 33, 'm': 'test2'},
        {'x': 3, 'y': 'val', 'z': 54, 'm': 'test'}  # Duplicate of first value
    ]

    schema = [
        {'name': 'hash', 'type': 'STRING'},
        {'name': 'load_date', 'type': 'TIMESTAMP'},
        {'name': 'record_source', 'type': 'STRING'},
        {'name': 'xyz_hash', 'type': 'STRING'},
        {'name': 'm_hash', 'type': 'STRING'},
    ]

    input_bucket = 'gs://test'
    output_bucket = 'gs://test'

    input_object = 'input_file'
    output_object = 'output_file'

    business_keys = [['x', 'y', 'z'], ['m']]

    load_date = '2018-10-01'
    record_source = 'test'

    def test_throw_error_with_business_key_lesser_then_two(self):
        with pytest.raises(RuntimeError):
            LinkFormatterOperator(
                task_id='test-link-formatter',
                input_bucket=self.input_bucket,
                output_bucket=self.input_bucket,
                input_object=self.input_object,
                output_object=self.output_object,
                business_keys=[['x']],
                load_date=self.load_date,
                record_source=self.record_source,
                schema=[field['name'] for field in self.schema]
            )

    @mock.patch('mapp.operators.dv_operators._get_data_from_gcs')
    @mock.patch('mapp.operators.dv_operators._send_data_to_gcs')
    def test_correct_result(self, mock_send_data_to_gcs, mock_get_data_from_gcs):
        tmp_file_download = NamedTemporaryFile(delete=False)
        for row in self.rows:
            s = json.dumps(row, sort_keys=True)
            s = s.encode('utf-8')
            tmp_file_download.write(s)
            tmp_file_download.write(b'\n')
        tmp_file_download.flush()

        mock_get_data_from_gcs.return_value = tmp_file_download.name

        ld = time.mktime(datetime.datetime.strptime(self.load_date, '%Y-%m-%d').timetuple())

        expected_values = [
            {'hash': '96424361b5c25cfb22e850c39fee55d0', 'load_date': ld, 'record_source': self.record_source,
             'xyz_hash': '4381762de0a9a0eb3dd21ab1eac02f30', 'm_hash': 'fb469d7ef430b0baf0cab6c436e70375'},

            {'hash': '19061a38282a94282c3b21751e1f6ee7', 'load_date': ld, 'record_source': self.record_source,
             'xyz_hash': '05dce42b0d7fe0f274448e02c1cbf673', 'm_hash': 'fb469d7ef430b0baf0cab6c436e70375'},

            {'hash': 'd5174c153e792afb9e523a1282d0719c', 'load_date': ld, 'record_source': self.record_source,
             'xyz_hash': '9d874d0b78dba5ae768fa71d27b17c59', 'm_hash': '4ff9018a647ae315a7e6601a818b4940'},
        ]

        def _assert_send_correct_file_to_gcs(gcp_conn_id, bucket, output, tmp_filename):
            assert bucket, self.output_bucket
            assert output, self.output_object

            expected = pandas.DataFrame(expected_values).to_json(orient='records', lines=True)

            with open(tmp_filename) as f:
                assert f.read() == expected

        mock_send_data_to_gcs.side_effect = _assert_send_correct_file_to_gcs

        op = LinkFormatterOperator(
            task_id='test-link-formatter',
            input_bucket=self.input_bucket,
            output_bucket=self.input_bucket,
            input_object=self.input_object,
            output_object=self.output_object,
            business_keys=self.business_keys,
            load_date=self.load_date,
            record_source=self.record_source,
            schema=[field['name'] for field in self.schema]
        )

        op.execute(None)


class TestSatelliteFormatterOperator(object):
    rows = [
        {'x': 3, 'y': 'val', 'z': 54, 'd1': 3, 'd2': 'val'},
        {'x': 1, 'y': 'lav', 'z': 108, 'd1': 12, 'd2': 'vxl'},
        {'x': 2, 'y': 'val', 'z': 33, 'd1': 10, 'd2': 'vxl'},
        {'x': 3, 'y': 'val', 'z': 54, 'd1': 3, 'd2': 'val'}  # Duplicate of first value
    ]

    schema = [
        {'name': 'hash', 'type': 'STRING'},
        {'name': 'load_date', 'type': 'TIMESTAMP'},
        {'name': 'record_source', 'type': 'STRING'},
        {'name': 'd1', 'type': 'INTEGER'},
        {'name': 'd2', 'type': 'STRING'},
        {'name': 'checksum', 'type': 'STRING'}
    ]

    input_bucket = 'gs://test'
    output_bucket = 'gs://test'

    input_object = 'input_file'
    output_object = 'output_file'

    business_keys = [['x', 'y', 'z']]
    checksum_keys = ['d1', 'd2']
    load_date = '2018-10-01'
    record_source = 'test'

    @mock.patch('mapp.operators.dv_operators._get_data_from_gcs')
    @mock.patch('mapp.operators.dv_operators._send_data_to_gcs')
    def test_correct_result_for_hubs(self, mock_send_data_to_gcs, mock_get_data_from_gcs):
        tmp_file_download = NamedTemporaryFile(delete=False)

        for row in self.rows:
            s = json.dumps(row, sort_keys=True)
            s = s.encode('utf-8')
            tmp_file_download.write(s)
            tmp_file_download.write(b'\n')
        tmp_file_download.flush()

        mock_get_data_from_gcs.return_value = tmp_file_download.name

        ld = time.mktime(datetime.datetime.strptime(self.load_date, '%Y-%m-%d').timetuple())

        expected_values = [
            {'hash': '4381762de0a9a0eb3dd21ab1eac02f30', 'load_date': ld, 'record_source': self.record_source,
             'd1': 3,
             'd2': 'val', 'checksum': 'a9ee116e2392718dbc1260091728f59c'},
            {'hash': '05dce42b0d7fe0f274448e02c1cbf673', 'load_date': ld, 'record_source': self.record_source,
             'd1': 12,
             'd2': 'vxl', 'checksum': 'b862fc51270a5750267d7fbce87ce814'},
            {'hash': '9d874d0b78dba5ae768fa71d27b17c59', 'load_date': ld, 'record_source': self.record_source,
             'd1': 10,
             'd2': 'vxl', 'checksum': '8dfb36f4214c3ac520b33fe8700c3a12'}
        ]

        def _assert_send_correct_file_to_gcs(gcp_conn_id, bucket, output, tmp_filename):
            assert bucket, self.output_bucket
            assert output, self.output_object

            expected = pandas.DataFrame(expected_values).to_json(orient='records', lines=True)

            with open(tmp_filename) as f:
                assert f.read() == expected

        mock_send_data_to_gcs.side_effect = _assert_send_correct_file_to_gcs

        op = SatelliteFormatterOperator(
            task_id='test-satellite-formatter',
            input_bucket=self.input_bucket,
            output_bucket=self.input_bucket,
            input_object=self.input_object,
            output_object=self.output_object,
            business_keys=self.business_keys,
            load_date=self.load_date,
            record_source=self.record_source,
            checksum_keys=self.checksum_keys,
            link_mode=False,
            schema=[field['name'] for field in self.schema]
        )

        op.execute(None)

        tmp_file_download.close()

    @mock.patch('mapp.operators.dv_operators._get_data_from_gcs')
    @mock.patch('mapp.operators.dv_operators._send_data_to_gcs')
    def test_correct_result_for_links(self, mock_send_data_to_gcs, mock_get_data_from_gcs):
        tmp_file_download = NamedTemporaryFile(delete=False)

        for row in self.rows:
            s = json.dumps(row, sort_keys=True)
            s = s.encode('utf-8')
            tmp_file_download.write(s)
            tmp_file_download.write(b'\n')
        tmp_file_download.flush()

        mock_get_data_from_gcs.return_value = tmp_file_download.name

        ld = time.mktime(datetime.datetime.strptime(self.load_date, '%Y-%m-%d').timetuple())

        expected_values = [
            {'hash': '4381762de0a9a0eb3dd21ab1eac02f30', 'load_date': ld, 'record_source': self.record_source,
             'd1': 3,
             'd2': 'val', 'checksum': 'a9ee116e2392718dbc1260091728f59c'},
            {'hash': '05dce42b0d7fe0f274448e02c1cbf673', 'load_date': ld, 'record_source': self.record_source,
             'd1': 12,
             'd2': 'vxl', 'checksum': 'b862fc51270a5750267d7fbce87ce814'},
            {'hash': '9d874d0b78dba5ae768fa71d27b17c59', 'load_date': ld, 'record_source': self.record_source,
             'd1': 10,
             'd2': 'vxl', 'checksum': '8dfb36f4214c3ac520b33fe8700c3a12'}
        ]

        def _assert_send_correct_file_to_gcs(gcp_conn_id, bucket, output, tmp_filename):
            assert bucket, self.output_bucket
            assert output, self.output_object

            expected = pandas.DataFrame(expected_values).to_json(orient='records', lines=True)

            with open(tmp_filename) as f:
                assert f.read() == expected

        mock_send_data_to_gcs.side_effect = _assert_send_correct_file_to_gcs

        op = SatelliteFormatterOperator(
            task_id='test-satellite-formatter',
            input_bucket=self.input_bucket,
            output_bucket=self.input_bucket,
            input_object=self.input_object,
            output_object=self.output_object,
            business_keys=self.business_keys,
            load_date=self.load_date,
            record_source=self.record_source,
            checksum_keys=self.checksum_keys,
            link_mode=False,
            schema=[field['name'] for field in self.schema]
        )

        op.execute(None)

        tmp_file_download.close()


class TestDatavaultForeignKeyCheck(object):

    def test_construct_correct_check_query(self):
        op = DatavaultForeignKeyCheckOperator(task_id='test',
                                              bigquery_conn_id='default',
                                              destination_dataset_id='test',
                                              staging_dataset_id='staging',
                                              staging_table_id='staging_table',
                                              dependencies={'hash': {'table': 'reference', 'column': 'hash'},
                                                            'hash2': {'table': 'reference2', 'column': 'hash2'}
                                                            }
                                              )

        expected_sql = """
         SELECT
            COUNT(1) = SUM(CASE WHEN reference.hash IS NULL THEN 0  ELSE 1 END) 
                AS reference__hash,
            COUNT(1) = SUM(CASE WHEN reference2.hash2 IS NULL THEN 0 ELSE 1 END) 
                AS reference2__hash2 
        FROM staging.staging_table 
            LEFT JOIN test.reference 
                ON staging_table.hash = reference.hash
            LEFT JOIN test.reference2 
                ON staging_table.hash2 = reference2.hash2
        """

        assert op.dependency_check_sql.replace('\n', '').replace(' ', '') == expected_sql.replace('\n', '').replace(
            ' ',
            '')
