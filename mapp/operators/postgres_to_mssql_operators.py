import logging
import pandas as pd
from tempfile import NamedTemporaryFile

from airflow.models import BaseOperator
from airflow.plugins_manager import AirflowPlugin
from airflow.utils.decorators import apply_defaults
from airflow.contrib.hooks.gcs_hook import GoogleCloudStorageHook
from airflow.hooks.mssql_hook import MsSqlHook
from airflow.hooks.postgres_hook import PostgresHook
from sqlalchemy import create_engine


class PostgresToMssqlOperator(BaseOperator):
    template_fields = ('postgres_sql', 'mssql_table_name')
    template_ext = ('.sql',)
    ui_color = '#a0e08c'

    @apply_defaults
    def __init__(self,
                 postgres_sql: str,
                 mssql_table_name: str,
                 mssql_conn_id: str = 'mssql_default',
                 postgres_conn_id: str = 'postgres_default',
                 if_exists='append',
                 *args,
                 **kwargs):
        super(PostgresToMssqlOperator, self).__init__(*args, **kwargs)

        self.postgres_sql = postgres_sql
        self.mssql_table_name = mssql_table_name

        self.mssql_conn_id = mssql_conn_id
        self.postgres_conn_id = postgres_conn_id

        self.if_exists = if_exists

    def execute(self, context):
        data = self._get_data_from_postgres()

        self._send_data_to_mssql(data)

        logging.info('Transfer was a success')

    def _get_data_from_postgres(self):
        self.log.info(f'Getting postgres hook from connection id {self.postgres_conn_id}')
        postgres = PostgresHook(postgres_conn_id=self.postgres_conn_id)
        conn = postgres.get_conn()

        self.log.info(f'Connection created: {conn}')

        self.log.info('Executing sql query in created connection')

        return pd.read_sql(self.postgres_sql, conn)

    def _send_data_to_mssql(self, data: pd.DataFrame):
        self.log.info(f'Getting mssql hook from connection id {self.mssql_conn_id}')
        db = MsSqlHook.get_connection('gmcore_conn')

        engine = create_engine(f'mssql+pymssql://{db.login}:{db.password}@{db.host}:{db.port}/{db.schema}')
        self.log.info(f'Engine created: {engine}')

        self.log.info(f'Sending data to table {self.mssql_table_name}')
        data.to_sql(self.mssql_table_name, engine, if_exists=self.if_exists, index=False)
