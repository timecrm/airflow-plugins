import json
from tempfile import NamedTemporaryFile

from airflow.contrib.operators.postgres_to_gcs_operator import PostgresToGoogleCloudStorageOperator
from airflow.utils import apply_defaults


class PostgresToGCSOperator(PostgresToGoogleCloudStorageOperator):
    """
    This class behaviors like :class:`~airflow.contrib.operators.postgres_to_gcs_operator.PostgresToGoogleCloudStorageOperator`.

    The only difference is this class gives the option to send empty data
    """

    template_fields = ('sql', 'bucket', 'filename', 'schema_filename', 'parameters')
    template_ext = ('.sql',)
    ui_color = '#a0e08c'

    @apply_defaults
    def __init__(self,
                 sql,
                 bucket,
                 filename,
                 schema_filename=None,
                 approx_max_file_size_bytes=1900000000,
                 postgres_conn_id='postgres_default',
                 google_cloud_storage_conn_id='google_cloud_default',
                 delegate_to=None,
                 parameters=None,
                 send_empty_data=False,
                 *args,
                 **kwargs):
        """
        :param sql: The SQL to execute on the Postgres table.
        :type sql: str
        :param bucket: The bucket to upload to.
        :type bucket: str
        :param filename: The filename to use as the object name when uploading
            to Google Cloud Storage. A {} should be specified in the filename
            to allow the operator to inject file numbers in cases where the
            file is split due to size.
        :type filename: str
        :param schema_filename: If set, the filename to use as the object name
            when uploading a .json file containing the BigQuery schema fields
            for the table that was dumped from Postgres.
        :type schema_filename: str
        :param approx_max_file_size_bytes: This operator supports the ability
            to split large table dumps into multiple files (see notes in the
            filenamed param docs above). Google Cloud Storage allows for files
            to be a maximum of 4GB. This param allows developers to specify the
            file size of the splits.
        :type approx_max_file_size_bytes: long
        :param postgres_conn_id: Reference to a specific Postgres hook.
        :type postgres_conn_id: str
        :param google_cloud_storage_conn_id: Reference to a specific Google
            cloud storage hook.
        :type google_cloud_storage_conn_id: str
        :param delegate_to: The account to impersonate, if any. For this to
            work, the service account making the request must have domain-wide
            delegation enabled.
        :param parameters: a parameters dict that is substituted at query runtime.
        :type parameters: dict
        :param send_empty_data: If true, sends a empty result to google cloud storage
        :type send_empty_data: bool
        """

        super(PostgresToGCSOperator, self).__init__(
            sql=sql,
            bucket=bucket,
            filename=filename,
            schema_filename=schema_filename,
            approx_max_file_size_bytes=approx_max_file_size_bytes,
            postgres_conn_id=postgres_conn_id,
            google_cloud_storage_conn_id=google_cloud_storage_conn_id,
            delegate_to=delegate_to,
            parameters=parameters,
            *args,
            **kwargs
        )

        self.send_empty_data = send_empty_data

    def _write_local_data_files(self, cursor):
        schema = list(map(lambda schema_tuple: schema_tuple[0], cursor.description))
        tmp_file_handles = {}
        row_no = 0

        def _create_new_file():
            handle = NamedTemporaryFile(delete=True)
            filename = self.filename.format(len(tmp_file_handles))
            tmp_file_handles[filename] = handle
            return handle

        if cursor.rowcount > 0 or self.send_empty_data:
            tmp_file_handle = _create_new_file()

            for row in cursor:
                # Convert datetime objects to utc seconds, and decimals to floats
                row = map(self.convert_types, row)
                row_dict = dict(zip(schema, row))

                s = json.dumps(row_dict, sort_keys=True).encode('utf-8')
                tmp_file_handle.write(s)

                # Append newline to make dumps BigQuery compatible.
                tmp_file_handle.write(b'\n')

                # Stop if the file exceeds the file size limit.
                if tmp_file_handle.tell() >= self.approx_max_file_size_bytes:
                    tmp_file_handle = _create_new_file()
                row_no += 1

        self.log.info('Received %s rows over %s files', row_no, len(tmp_file_handles))

        return tmp_file_handles
