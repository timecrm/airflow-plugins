import datetime
import hashlib
import json
import os
import pandas as pd
import time
from airflow import AirflowException
from airflow.contrib.hooks.bigquery_hook import BigQueryHook
from airflow.contrib.hooks.gcs_hook import GoogleCloudStorageHook
from airflow.operators import BaseOperator
from airflow.plugins_manager import AirflowPlugin
from airflow.utils.decorators import apply_defaults
from tempfile import NamedTemporaryFile
from typing import List, Any, Dict


def _get_data_from_gcs(gcp_conn_id, bucket, input):
    tmp_file = NamedTemporaryFile(delete=False)
    hook = GoogleCloudStorageHook(google_cloud_storage_conn_id=gcp_conn_id)
    hook.download(bucket, input, tmp_file.name)
    return tmp_file.name


def _get_key_values_as_lists(dictionary: dict, keys: list):
    """Retorna lista de lista dos valores de um dicionario"""
    return [[dictionary[v] for v in obj] if isinstance(obj, list) else [dictionary[obj]] for obj in keys]


def _convert_arr_to_str_arr(arr):
    """Convert uma lista de listas para lista de listas com todos os objetos como string"""
    return [[str(v) for v in subarr] for subarr in arr]


def generate_hash_from_str_list(str_list: List[str]) -> str:
    return hashlib.md5('|'.join(sorted(str_list)).encode('utf-8')).hexdigest()


def generate_hash_from_keys(row: Dict[str, Any], keys: List[Any]) -> (str, List[Any]):
    """
    Gera uma hash a partir de uma lista de lista de chaves.
    :param row:
        Dicitionário de valores
    :param keys:
        Chaves específicas para buscar em 'row'
    :return:
        A hash das chaves requeridas
    """
    key_values = _get_key_values_as_lists(row, keys)

    str_key_values = _convert_arr_to_str_arr(key_values)

    # Computa hash individuais de cada chave
    key_hashs = [hashlib.md5('|'.join(sorted(b)).encode('utf-8')).hexdigest() for b in str_key_values]

    final_hash = hashlib.md5('|'.join(sorted(key_hashs)).encode('utf-8')).hexdigest()

    return final_hash, key_hashs, key_values


def _send_data_to_gcs(gcp_conn_id, bucket, output, filename):
    hook = GoogleCloudStorageHook(google_cloud_storage_conn_id=gcp_conn_id)
    hook.upload(bucket, output, filename=filename, mime_type='application/json')


class BaseDatavaultFormatterOperator(BaseOperator):
    template_fields = ('input_bucket', 'output_bucket', 'output_object', 'input_object', 'load_date')

    @apply_defaults
    def __init__(
            self,
            input_bucket: str,
            output_bucket: str,
            input_object: str,
            output_object: str,
            business_keys: List[Any],
            record_source: str,
            schema: List[str],
            checksum_keys: List[Any] = None,
            load_date: str = '{{ds}}',
            gcp_conn_id='google_cloud_default',
            *args,
            **kwargs
    ):
        super(BaseDatavaultFormatterOperator, self).__init__(*args, **kwargs)

        self.gcp_conn_id = gcp_conn_id
        self.input_bucket = input_bucket
        self.output_bucket = output_bucket
        self.input_object = input_object
        self.output_object = output_object
        self.business_keys = business_keys
        self.checksum_keys = checksum_keys
        self.record_source = record_source
        self.load_date = load_date
        self.schema = schema

    def execute(self, context):
        self.log.info('Getting data from GCS')
        download_filename = _get_data_from_gcs(self.gcp_conn_id, self.input_bucket, self.input_object)

        self.log.info('Processing file lines')
        with open(download_filename) as f:
            formatted_rows = [self._format_row(json.loads(line)) for line in f]
        cleaned_dataframe = pd.DataFrame(formatted_rows, columns=self.schema).drop_duplicates()
        self.log.info(f'Generated {len(cleaned_dataframe)} candidates.')

        self.log.info('Sending processed data to GCS')
        upload_file = NamedTemporaryFile(delete=False)
        cleaned_dataframe = cleaned_dataframe[sorted(cleaned_dataframe.columns)]
        cleaned_dataframe.to_json(upload_file.name, orient='records', lines=True)
        _send_data_to_gcs(self.gcp_conn_id, self.output_bucket, self.output_object, upload_file.name)

        self.log.info('Cleaning temporary files')
        os.remove(download_filename)
        upload_file.close()
        os.remove(upload_file.name)

    def _format_row(self, row):
        raise NotImplementedError


class HubFormatterOperator(BaseDatavaultFormatterOperator):
    """
    Formats a GCS Object to Datavault Hub Format and send back to GCS in new format.
    """

    template_fields = ('input_bucket', 'output_bucket', 'output_object', 'input_object', 'load_date')

    @apply_defaults
    def __init__(
            self,
            input_bucket: str,
            output_bucket: str,
            input_object: str,
            output_object: str,
            business_keys: List[Any],
            record_source: str,
            schema: List[str],
            load_date: str = '{{ds}}',
            gcp_conn_id='google_cloud_default',
            *args,
            **kwargs
    ):
        if len(business_keys) > 1:
            raise RuntimeError('Hub business key list must be of size 1.')

        super(HubFormatterOperator, self).__init__(
            input_bucket=input_bucket,
            output_bucket=output_bucket,
            input_object=input_object,
            output_object=output_object,
            business_keys=business_keys,
            record_source=record_source,
            schema=schema,
            load_date=load_date,
            gcp_conn_id=gcp_conn_id,
            checksum_keys=None,
            *args,
            **kwargs)

    def _format_row(self, row):
        key_values = _get_key_values_as_lists(row, self.business_keys)
        str_key_values = _convert_arr_to_str_arr(key_values)
        intermediary_hashes = [generate_hash_from_str_list(s) for s in str_key_values]
        final_hash = generate_hash_from_str_list(intermediary_hashes)

        row = [final_hash,
               time.mktime(datetime.datetime.strptime(self.load_date, "%Y-%m-%d").timetuple()),
               self.record_source
               ] + [v for sublist in key_values for v in sublist]

        # Flattens the list
        return row


class SatelliteFormatterOperator(BaseDatavaultFormatterOperator):
    """
    Formats a GCS JSON file to Datavault Satellite Style and Send Back to GCS in the new format
    """

    template_fields = ('input_bucket', 'output_bucket', 'output_object', 'input_object', 'load_date')

    @apply_defaults
    def __init__(self,
                 input_bucket: str,
                 output_bucket: str,
                 input_object: str,
                 output_object: str,
                 business_keys: List[Any],
                 checksum_keys: List[Any],
                 record_source: str,
                 schema: List[str],
                 load_date: str = '{{ds}}',
                 gcp_conn_id: str = 'google_cloud_default',
                 link_mode=False,
                 *args,
                 **kwargs
                 ):
        super(SatelliteFormatterOperator, self).__init__(
            input_bucket=input_bucket,
            output_bucket=output_bucket,
            input_object=input_object,
            output_object=output_object,
            business_keys=business_keys,
            record_source=record_source,
            schema=schema,
            load_date=load_date,
            gcp_conn_id=gcp_conn_id,
            checksum_keys=checksum_keys,
            *args,
            **kwargs)

        self.link_mode = link_mode

    def _format_row(self, row):
        # Business Keys
        bk_key_values = _get_key_values_as_lists(row, self.business_keys)
        bk_str_key_values = _convert_arr_to_str_arr(bk_key_values)

        if self.link_mode:
            # Compute hashes like a link
            intermediary_bk_hashes = \
                [generate_hash_from_str_list([generate_hash_from_str_list(s)]) for s in bk_str_key_values]
        else:
            # Compute hashes like a Hub
            intermediary_bk_hashes = [generate_hash_from_str_list(s) for s in bk_str_key_values]

        bk_final_hash = generate_hash_from_str_list(intermediary_bk_hashes)

        # Checksum
        chk_key_values = _get_key_values_as_lists(row, self.checksum_keys)
        chk_str_key_values = _convert_arr_to_str_arr(chk_key_values)
        intermediary_chk_hashes = [generate_hash_from_str_list(s) for s in chk_str_key_values]
        chk_final_hash = generate_hash_from_str_list(intermediary_chk_hashes)

        flatten_descriptive_values = [v for sublist in chk_key_values for v in sublist]

        return [
                   bk_final_hash,
                   time.mktime(datetime.datetime.strptime(self.load_date, "%Y-%m-%d").timetuple()),
                   self.record_source
               ] + flatten_descriptive_values + [chk_final_hash]


class LinkFormatterOperator(BaseDatavaultFormatterOperator):
    """
    Formats a GCS to Datavault Link and send back to GCS in new Format
    """

    template_fields = ('input_bucket', 'output_bucket', 'output_object', 'input_object', 'load_date')

    @apply_defaults
    def __init__(
            self,
            input_bucket: str,
            output_bucket: str,
            input_object: str,
            output_object: str,
            business_keys: List[List[str]],
            record_source: str,
            schema: List[str],
            load_date: str = '{{ds}}',
            gcp_conn_id='google_cloud_default',
            *args,
            **kwargs
    ):
        if len(business_keys) < 2:
            raise RuntimeError('Link business key list must be of size larger than 1.')

        super(LinkFormatterOperator, self).__init__(
            input_bucket=input_bucket,
            output_bucket=output_bucket,
            input_object=input_object,
            output_object=output_object,
            business_keys=business_keys,
            record_source=record_source,
            schema=schema,
            load_date=load_date,
            gcp_conn_id=gcp_conn_id,
            checksum_keys=None,
            *args,
            **kwargs)

    def _format_row(self, row):
        # Business Keys
        bk_key_values = _get_key_values_as_lists(row, self.business_keys)
        bk_str_key_values = _convert_arr_to_str_arr(bk_key_values)
        intermediary_bk_hashes = [generate_hash_from_str_list([generate_hash_from_str_list(s)]) for s in
                                  bk_str_key_values]
        bk_final_hash = generate_hash_from_str_list(intermediary_bk_hashes)

        return [
                   bk_final_hash,
                   time.mktime(datetime.datetime.strptime(self.load_date, "%Y-%m-%d").timetuple()),
                   self.record_source
               ] + intermediary_bk_hashes


class DatavaultForeignKeyCheckOperator(BaseOperator):
    template_fields = ('dependency_check_sql',)

    join_template = """
        LEFT JOIN {dataset}.{dependency_table} 
            ON {dependant_table}.{dependant_column} = {dependency_table}.{dependency_column}
    """

    check_template = """
        COUNT(1) = SUM(CASE WHEN {dependency_table}.{dependency_column} IS NULL THEN 0 ELSE 1 END) 
            AS {dependency_table}__{dependency_column} 
    """

    dependency_check_sql_template = """
        SELECT
            {check_template}
        FROM {dataset}.{dependant_table} 
            {join_template}
    """

    @apply_defaults
    def __init__(self,
                 destination_dataset_id: str,
                 staging_dataset_id: str,
                 staging_table_id: str,
                 dependencies: dict,
                 bigquery_conn_id: str = 'google_cloud_default',
                 *args,
                 **kwargs
                 ):
        super(DatavaultForeignKeyCheckOperator, self).__init__(*args, **kwargs)

        self.bigquery_conn_id = bigquery_conn_id
        self.destination_dataset_id = destination_dataset_id
        self.staging_dataset_id = staging_dataset_id
        self.staging_table_id = staging_table_id
        self.dependencies = dependencies

        joins = []
        checks = []
        for dep in self.dependencies:
            joins.append(self.join_template.format(dataset=self.destination_dataset_id,
                                                   dependant_table=self.staging_table_id,
                                                   dependant_column=dep,
                                                   dependency_table=self.dependencies[dep]['table'],
                                                   dependency_column=self.dependencies[dep]['column']
                                                   ))

            checks.append(self.check_template.format(
                dependency_table=self.dependencies[dep]['table'],
                dependency_column=self.dependencies[dep]['column']))

        self.dependency_check_sql = self.dependency_check_sql_template.format(
            dependant_table=self.staging_table_id,
            dataset=self.staging_dataset_id,
            check_template=','.join(checks),
            join_template=''.join(joins))

    def execute(self, context):

        hook = BigQueryHook(bigquery_conn_id=self.bigquery_conn_id, use_legacy_sql=False)
        conn = hook.get_conn()
        cursor = conn.cursor()

        self.log.info('Executing check query:\n%s' % self.dependency_check_sql)
        cursor.execute(self.dependency_check_sql)
        result = cursor.fetchall()[0]

        error_message = []
        for i, is_ok in enumerate(result):
            if not is_ok:
                error_message.append('dependency %d contains some references in staging which do not exists.' % (i + 1))
        if error_message:
            raise AirflowException('\n'.join(error_message))

        self.log.info('Dependencies checked.')


class DatavaultInsertOperator(BaseOperator):
    """
    """
    template_fields = ('staging_project_dataset_table_id',)

    def __init__(self,
                 bigquery_conn_id,
                 destination_project_dataset_table_id: str,
                 staging_project_dataset_table_id: str,
                 mode: str = 'hub',
                 schema: List[str] = None,
                 *args,
                 **kwargs
                 ):
        """
        :param destination_project_dataset_table_id:
            Where to insert the data in bigquery
        :param staging_project_dataset_table_id:
            Where staging data is located
        :param mode:
            Mode of insertion, if 'hub' and 'link', use only the table hash as check. If 'satellite' uses
            hash, load_date and checksum.
        :param schema:
            The schema of `hub_project_dataset_table_id`. If None, infer from BigQuery itself. The schema must have
            same order of columns as table and hub hash key MUST BE first column.
        :param args:
        :param kwargs:
        """
        super(DatavaultInsertOperator, self).__init__(*args, **kwargs)

        self.bigquery_conn_id = bigquery_conn_id
        self.destination_project_dataset_table_id = destination_project_dataset_table_id
        self.staging_project_dataset_table_id = staging_project_dataset_table_id

        self.schema = schema

        self.sql = """
            INSERT INTO `{table}` ({cols})
            SELECT DISTINCT 
                S.*
            FROM `{staging}` AS S 
                LEFT JOIN `{table}` AS T
                    ON S.{hash} = T.{hash}
            WHERE T.{hash} IS NULL 
        """

        # Insersão de satelites é um pouco diferente
        if mode == 'satellite':
            self.sql = """  
            INSERT INTO `{table}` ({cols})          
            WITH
              most_recent_sat AS (
                SELECT DISTINCT
                    * EXCEPT (rank)
                FROM (
                    SELECT
                        *,
                        RANK() OVER (PARTITION BY {hash} ORDER BY load_date DESC) AS rank
                    FROM `{table}` 
                    )
                WHERE rank = 1 
                )
                SELECT
                    DISTINCT S.*
                FROM `{staging}` AS S
                    LEFT JOIN `most_recent_sat` AS T
                        ON  S.{hash} = T.{hash}
                WHERE T.{hash} IS NULL OR (T.{hash} IS NOT NULL AND S.checksum <> T.checksum)
            """

        self.hook = None
        self.conn = None
        self.cursor = None

    def execute(self, context):
        # TODO Buscar schema do hub se não for passado schema via argumento

        sql = self.sql.format(
            table=self.destination_project_dataset_table_id,
            staging=self.staging_project_dataset_table_id,
            cols=','.join(self.schema),
            hash=self.schema[0]
        )

        self.log.info('Execution sql \n {}'.format(sql))

        self.hook = BigQueryHook(bigquery_conn_id=self.bigquery_conn_id, use_legacy_sql=False)

        self.conn = self.hook.get_conn()

        self.cursor = self.conn.cursor()

        self.cursor.run_query(sql)

    def on_kill(self):
        super(DatavaultInsertOperator, self).on_kill()
        if self.cursor is not None:
            self.log.info('Cancelling running query')
            self.cursor.cancel_query()


class MappDvPlugin(AirflowPlugin):
    name = 'mapp_dv_operators'

    operators = [
        DatavaultInsertOperator,
        HubFormatterOperator,
        SatelliteFormatterOperator,
        LinkFormatterOperator,
        DatavaultForeignKeyCheckOperator
    ]
